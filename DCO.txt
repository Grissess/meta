This agreement applies to the Robigalia project as of 2016-01-02T12:44:55Z. By
signing this document, you certify the conditions below for all contributions
submitted to the Robigalia project.

To sign this agreement, create an "ASCII-armored" PGP signature of this file
and place it in the `agreements` directory. The file name should be the same
as your GitLab username, if you have one. If you do not, naming it after your
email address is acceptable.

As an example, when cmr signed this document, they ran the shell command:

    gpg --sign --armor < DCO.txt > agreements/cmr

If you have any questions, email <robigalia-dev@lists.robigalia.org> or
<corey@octayn.net>.

===============================================================================

Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
660 York Street, Suite 102,
San Francisco, CA 94110 USA

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.

