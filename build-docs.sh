#!/usr/bin/env bash

if [ ! -x $(which pandoc) ]; then
    echo "Install pandoc plz"
    exit 1
fi

if [ ! -x $(which cleancss) ]; then
    echo "npm install clean-css plz"
    exit 1
fi

pandoc_it() {
    pandoc -f markdown -t html5 -B docs/template-before.html \
        -A docs/template-after.html $1 -o output/$2
}

mkdir -p output
cleancss docs/style.css -o output/style.css
for file in docs/*.md; do
    pandoc_it $file $(basename -s .md $file).html
done

pandoc_it CONDUCT.md conduct.html
pandoc_it CONTRIBUTING.md contributing.html
pandoc_it DCO.txt dco.html

cp output/docs.html output/index.html
