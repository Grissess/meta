# Microkernels and seL4

What's this about microkernels? Didn't [Linux kill that
idea](https://en.wikipedia.org/wiki/Tanenbaum%E2%80%93Torvalds_debate)? Not
at all! The L4 family of microkernels is [widely
deployed](https://en.wikipedia.org/wiki/L4_microkernel_family#Commercial_deployment)
in applications where the benefits of microkernels are critical: security
combined with excellent performance and worst-case execution time.

The introduction to the [L4 users
manual](http://l4hq.org/docs/manuals/l4uman.pdf) states the fundamental
principle of good micro kernel design:

> [T]he main design criterion of the micro-kernel is minimality with respect
> to security: *A service (feature) is to be included in the micro-kernel if
> and only if it is impossible to provide that service outside the kernel
> without loss of security.* The idea is that once we make things small (and
> do it well), performance will look after itself.

seL4 is unique in that it is the only kernel ever to have an end-to-end
formal, machine-checked proof of implementation correctness and security
enforcement. See [its
brochure](http://sel4.systems/Info/Docs/seL4-brochure.pdf) for a summary, or
the conference paper [From L3 to seL4 - what have we learnt in 20 years of L4
microkernels](http://ssrg.nicta.com.au/publications/nictaabstracts/Elphinstone_Heiser_13.abstract.pml)
for a more detailed description.

The fundamental features seL4 provides are thread management, scheduling, and
inter-process communication (IPC). On top of this, all else comes. It is up to
userland to implement device drivers and higher-level operating system
functionality such as the POSIX API.

Together with Rust, seL4 can be a powerful weapon in the war to secure the
Internet of Things.
