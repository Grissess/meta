# Roadmap

We're currently in the early phases of development, with a design not yet
settled on. We are currently focusing on the lower-level aspects of seL4
development and trying to create higher-level abstractions that are easier to
use and build systems on.

Here's our concrete goals at the moment:

1. ELF loading and dynamic linker
2. Networking stack able to run [pnet](https://github.com/libpnet/libpnet) on.
3. TCP/IP stack on pnet
4. Complete virtio drivers
5. Some filesystem
6. Basic POSIX support to be able to run [busybox](https://busybox.net/)

These are a start, but not the end.

## Current State

Right now, only the absolute minimum for using Rust on seL4 is supported:
building static binaries that can run as the initial thread at boot. See
[hello-world](https://gitlab.com/robigalia/hello-world) for an example of
this.

# Dashboard

These are the crates we're currently developing. See their project pages for
more information, documentation links, and an issue tracker.

- [sel4-sys](https://gitlab.com/robigalia/sel4-sys)
- [rust-sel4](https://gitlab.com/robigalia/rust-sel4)
- [sel4-start](https://gitlab.com/robigalia/sel4-start)
- [pci](https://gitlab.com/robigalia/pci)
- [virtio](https://gitlab.com/robigalia/virtio)
- [hello-world](https://gitlab.com/robigalia/hello-world)
