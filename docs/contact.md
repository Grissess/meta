We primarly use email and IRC to communicate. Please note the [code of conduct](https://robigalia.org/conduct.html).

# Mailing List

We have a mailing list
[here](https://lists.robigalia.org/listinfo/robigalia-dev), which has frequent
updates from the devs and occasional discussion.

# IRC

We have an [IRC channel](http://webchat.freenode.net/?channels=%23robigalia)
on Freenode, `#robigalia`, for more real-time communication.

# Mumble

We'll occasionally want to talk. For this, we use
[Mumble](http://wiki.mumble.info/wiki/Main_Page), an open source,
low-bandwidth audio chat program. The server [is
here](mumble://comm.cslabs.clarkson.edu/Robigalia?title=Root&version=1.2.0).
