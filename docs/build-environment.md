# Setting up a build environment

For a number of reasons, setting up a build environment for Robigalia can be
somewhat convoluted. Rust does not have built-in support for targeting seL4.
Libraries need to be built for cross-compiling. Some nightly features are also
needed (inline assembly) for interfacing with the kernel.

Ubuntu 14.04 is the only tested and supported build environment, though it is
likely that any Linux distribution will work. Windows and OS X are not
currently supported as build environments. BSDs have not been tested, but it
is likely that they will work with little modification.

There are two options: using the [Vagrant VM we supply][vm], or setting up
everything yourself. We recommend using the VM for ease, but recognize that
it's not a substitute for detailed documentation.

Read on if you want to know how to setup a build environment manually.

[vm]: https://gitlab.com/robigalia/devbox

## Rust compiler

First, you need suitable Rust and C compilers.

For Rust, any nightly compiler newer than 2015-12-28 should work. It *must* be
nightly, not beta or stable. The easiest way to install a suitable Rust is
[multirust-rs](https://github.com/Diggsey/multirust-rs), and running

    multirust default nightly-2015-12-28

You can also use the [rustup script](https://github.com/rust-lang/rustup) and

    rustup.sh --channel=nightly --date=2015-12-28 --prefix ~/.local

In either case, be sure to note the prefix that Rust is installed in, which
we'll call `$PREFIX` from here on. If you're using multirust, it's probably
`~/.multirust/toolchains/nightly`.

## C compiler + standard library

The targets you want to build for will determine which C compilers you need.
The C compiler is needed to build `compiler-rt` in the next step, and is used
as the linker.

The easiest thing is to install a clang and LLVM toolchain. You can run `llc
--version`, and if `x86` is listed, you are good to go!

You will also need C standard library header files for a
i686-unknown-linux-gnu. Only the freestanding headers are necessary. On Debian
and Debian-derivatives, you can `dpkg --add-architecture i386` and install
`libc6-dev:i386`. If you do not also want to cross-compile for ARM, you can
instead install `gcc-multilib`.

## Getting the target configuration

The [sel4-targets](https://gitlab.com/robigalia/sel4-targets) repository has
so-called "target specifications" for rustc. Clone this repository somewhere
and set the environment variable `RUST_TARGET_PATH` to that directory.

## Building compiler-rt and libcore

To get the source for the appropriate version of compiler-rt/libcore, look at
the `commit-date` field in `rustc -vV` and download

    http://static.rust-lang.org/dist/${commit-date}/rustc-nightly-src.tar.gz

Extract the archive.

Make the directory `$PREFIX/lib/rustlib/i686-sel4-unknown/lib`, such as with
`mkdir -p`.

    # compiler-rt
    pushd rustc-nightly/src/compiler-rt
    mkdir build
    make CC=clang ProjSrcRoot=$(pwd) ProjObjRoot=$(pwd)/build TargetTriple=i686-unknown-linux-gnu "CFLAGS=-target i686-unknown-linux-gnu -I/usr/include/i386-linux-gnu" triple-builtins
    cp build/triple/builtins/i386/libcompiler_rt.a $PREFIX/lib/rustlib/i686-sel4-unknown/lib/libcompiler-rt.a
    popd

    # libcore

    pushd rustc-nightly/src/libcore
    rustc -O --target=i686-sel4-unknown lib.rs
    cp libcore.rlib $PREFIX/lib/rustlib/i686-sel4-unknown/lib
    popd

Now you're set! You should be able to invoke either cargo or rustc with
`--target i686-sel4-unknown` now.

# Troubleshooting

If you tried following these steps but came across an error, **please** come
to our [IRC channel or mailing list](https://robigalia.org/contact.html) so we
can help you, and add more details to this section. Thanks!
