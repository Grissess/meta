# Introduction + docs

Robigalia is a project with two goals:

1. Build a robust Rust ecosystem around seL4
2. Create a secure POSIX-compatible userland on top of seL4.

We're currently working away at goal 1. In any case, to get started with Rust
and seL4, first [set up a build
environment](https://robigalia.org/build-environment.html).  Then, check out
the [tutorial](https://robigalia.org/tutorial.html) or
[examples](https://robigalia.org/examples.html) to see how you can start
building Rust applications.

[See here](https://robigalia.org/contributing.html) if you want to contribute
to any of the Robigalia-maintained libraries.

Want to know more about Rust, and why it's useful in this context? [Go
here](https://robigalia.org/why-rust.html).

Want to know more about microkernels and seL4? [We have that
too](https://robigalia.org/about-microkernels.html).

- [Current crates and roadmap](https://robigalia.org/dashboard.html)
- [Contributing](https://robigalia.org/contributing.html)
- [Certificate of origin](https://robigalia.org/dco.html)
- [Setting up a build environment](https://robigalia.org/build-environment.html)
- [Examples](https://robigalia.org/examples.html)
- [Tutorial](https://robigalia.org/tutorial.html)
