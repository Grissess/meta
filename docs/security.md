# Robigalia Security Policy

## Reporting a Bug

Safety is one of the core principles of Robigalia, and to that end, we would
like to ensure that Robigalia has a secure implementation. Thank you for taking
the time to responsibly disclose any issues you find.

All security bugs in any Robigalia crate should be reported by email to <a
href="mailto:corey@octayn.net">corey@octayn.net</a>. Your email will be
acknowledged within 24 hours, and you'll receive a more detailed response to
your email within 48 hours indicating the next steps in handling your report.
Please encrypt your report using my public key.  This key is [on MIT's
keyserver](https://pgp.mit.edu/pks/lookup?op=vindex&amp;search=0xF284F7A19456AE7C)
, <a href="#key">reproduced below</a>, and [on
keybase](https://keybase.io/cmr).

After the initial reply to your report, the security team will endeavor to keep
you informed of the progress being made towards a fix and full announcement. As
recommended by [RFPolicy](https://en.wikipedia.org/wiki/RFPolicy), these
updates will be sent at least every five days. In reality, this is more likely
to be every 24-48 hours.

If you have not received a reply to your email within 48 hours, or have not
heard from the security team for the past five days, post on the <a
href="https://lists.robigalia.org/listinfo/robigalia-dev">mailing list</a> or
ask in the #robigalia IRC room on irc.freenode.net.

Please note that the mailing lists and #robigalia IRC channel are
public areas. When escalating in these venues, please do not discuss your
issue. Simply say that you're trying to get a hold of someone from the security
team.

## Disclosure Policy

The Robigalia project has a 5 step disclosure process.

<ol>
<li>The security report is received and is assigned a primary handler. This
person will coordinate the fix and release process.</li>

<li>The problem is confirmed and a list of all affected versions is determined.</li>

<li>Code is audited to find any potential similar problems.</li>

<li>Fixes are prepared for all releases which are still under maintenance.
These fixes are not committed to the public repository but rather held locally
pending the announcement.</li>

<li>On the embargo date, the <a
href="https://lists.robigalia.org/listinfo/sec-ann"> Robigalia security
mailing list</a> is sent a copy of the announcement. The changes are pushed to
the public repository, Cargo packages published, and new builds are deployed
where applicable.</li> </ol>

This process can take some time, especially when coordination is required
with maintainers of other projects. Every effort will be made to handle the bug
in as timely a manner as possible, however it's important that we follow the
release process above to ensure that the disclosure is handled in a consistent
manner.

<h2>Receiving Security Updates</h2>

<p>The best way to receive all the security announcements is to subscribe to
the <a
href="https://lists.robigalia.org/listinfo/sec-ann">Robigalia
security announcements mailing list</a>. The mailing list is very low traffic,
and it receives the public notifications the moment the embargo is lifted.</p>

<h2>Comments on This Policy</h2>

<p>If you have any suggestions to improve this policy, please send an email to
<a href="mailto:corey@octayn.net">corey@octayn.net</a>.</p>

<h2 id="key">Plaintext PGP Key</h2>

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: GnuPG v2

    mQINBFYhlWEBEADBDu5WiWp64EMp510bo0f/8i89+YMLLLdmaWRvNEUqu8gYI5xu
    4kSoamBLfI2xOehXoRKus8N7tvhtp5vypTekpUlKP7hqUPtwMo9o09ZPKzwzKVto
    +dtGvoGDGokArrHJkmIK6jGWQc4bzDRMc7PjAWjpbpNombXvUCWPQA/TONrgR3xE
    5pe1+dq1ycc4vK42dHWXZ1UVzTDu20M38mocAuSq5uYs6umNIN2gBiauf4LGzIqJ
    t7q16JJvJIDKetoQ5DwLSpotDCB5/QE/U4S8VEFiuzgpG8C9jGx3rOo1zK6Q3DR0
    4Bm2FwZC9Q4wqAihsVlv28i62sZPo2W1ReL+9ERmPyr5FnmoTKyNOsmFU4XR/4fG
    ZSlKnKh+iDmXXsq572W0D8+wd373jSD3GaC6SWvonD1ahDLhxnYnjRRL9EbQkaBA
    BzgTr+qnVXxK5GOChq3be0x7RufBIhCZFAW/bERQ/h8YSe7cEPFBFGX6FugDvaWu
    6B7bl8OwzGFqWUq8fi/ZASB5wi1hBB6BJjZL23rBxmX72x6mSPbTfKxnESTPCO1i
    jfqF/pJNMgYboQ4t0mJQniQQgMD47FrUxCqp0H3sRZXZfB6nBbWYhl0QRBX9R3nm
    MWFhXjbqrd5diPflPObefc+ziOMpIls1n6qP++K4yqQwaaBZaDJ1xE4oyQARAQAB
    tCNDb3JleSBSaWNoYXJkc29uIDxjb3JleUBvY3RheW4ubmV0PokCMQQTAQoAGwUC
    ViGVYQIbAwUJAeEzgAILCQIVCgIeAQIXgAAKCRDyhPehlFaufBqLD/4rHfT3XQhW
    zjuzpG5YZyM6b8DSLzdVkmBJtvqGtEtdz08PMQYbJ3or1f67RZrg/9+n3SSiRrMy
    poM6c7LN24oQrld+fiVwkrYvOR5h+AGErTkzsbEfzBV1EU7OhTNTsq3JYlseyFkt
    YuWVwVRZedKcabqz2UZQjqplEIVatIxRlo1jEImAcFxgg77cfI78U2lJqxhZ+Sou
    RtE3HudjjtuURe8SaO/akhWmyEtlhP+++VEm/iHGnGDfbWGlacC7MQnqBw+dZc1g
    GwrasV8FNmKMm7ZM9LBVAyv7tNG9n/eNeLQwo+KsgDMDMhRE4D+lYSfYrrQOCIv7
    RiVp9ZT0Y4DkqrmuIsQ0oLcOLyccqE24wNhHu1Lh+r6Zxr08OlS6jjT8SeRkmzYN
    Ru/jpER9FPTrI26qJaPM+cHChr+xjuxt/qStMF4EWJqml4zXaBF7tWIrWQ+GaSVr
    Goe5b8cHK+7Ek29qPMWPXhNiMVPmwz87TKtJeLqwafHlF6SY7exjmqD+bDvGNCxP
    vk14pfr5QMKoGRRcKV39GCyh3tBVZDqEtwws6oYDSH6cwe5m1+QH70P6FxZKGU/T
    /MwrdKpx+y3qU04qIPaqAgx1yajWM5Dp3mpbum6Ba6dQjseyzzdojGmaMBxT5/xR
    BVF7t2ulgpqXJLW8mM4xi+Dts6Psz+lBJYkBHAQQAQoABgUCVjFyAgAKCRDrWY8v
    tp2G5SxWB/46blSJ6ohCOE+J3p+ptHMKS/aUMhq/w9C+bap6b5exVMKu80HYUoQQ
    czJVztNzU6GkqwLMCtHceByColY3CZ7eIKxFgkWIJKALiqkACf+EwrIMqzMITB7T
    xhQ4ODsbOshvDB9zS9dO3Qgv7+MwRJ0JICMXo1c02vdigPk/kKUYlVQzOpsTHAAb
    U5Li0jtWyOW03EwnNdqYxmc9/exhEz6P8P9rJHWgl0UdP7GiB7TMoPpKiXFYsr+i
    rptGulBry/jzxZifATRoUnBfmgBpOiMQbXkincrHHBcryX5fBJLJu0bVQ4TcDIz5
    TpW81+ZAx1Ar5W/o3lVz04FefAQ+HxvwiQIcBBABAgAGBQJWMX6DAAoJEGT9ju+p
    RDK2B+sP/1HD9joXagWSHP6dO9Z+n8m5o4I4cIqBAX0J3DzXrhPAE8Tgu0tNj2MO
    9c6WokoJ5MA93SKELfJjt06A1XTuHqHmJMlv9ld7LGUy1nhhBHEkjSpARevAxG64
    2CIsK0T2wqYvbj3ExJ/wvRNS+uZ7YFUU4HYM/tFAxnb7XAlrJj2NsTM42IHZvLEa
    9ccqeeBu72f3jfW8GYAsFJRDQuTuu/AY3W5KTFgSMBsmiP7QR28Hzjs0RZ/ReeWh
    iXeWigJCy6a6LvgE/CUZsHvIhxwaGzrlrrviT9eiaJvjrvSVr8vhkP/38LRKuOiN
    etPeVwrRGJNtAoOF0Jvfh9t0htWvG8Wtn//UujG94vM62AgcgBEJBdal89qS1KHo
    wwOss9cvpsWtUJLlHewoQS+HVj5Dwda7suiiet0hmTHfq3pcc/qCgwop7h94YE1a
    q02aZlDELM69mGfHrJQBvNohpvxkUl+E17ElTYzSO2097LvsfPdwqDq1fgVkG56Z
    lJvv6V2/K0zBgu07/2tB4mpkBJJ+8Y6SvsSnJxalwJki0WzO+6uOtnifIMAy463L
    9iEqOGwPU/UveykD/Zn1EJo3VxuL7mUI3At+zEqruXuLARq3xqvM2O2eDgFP9jPW
    jaF+/UtgD0t3CLXA9anoHdTPR4rJZC2yfDFV5hddO4l2t6rXrTaZiQIcBBABCAAG
    BQJWMXuzAAoJEAMf+cE6pOxU9joQAJamDGbyLOVOkEqvp2HrwLq812joO9JsPbMT
    ZBHy9tBZPTlj7QHNAnWWKR+n2SFlfy6k4o1Vh2O4+gQE7w3To88jyVr58LQGwhEK
    hjDnphTcvs7GZr7aDTTxPD8jUV3sdV+YvFA6TbDWAUin5uDFRfLzhuh7p+Ryhcv7
    ulVv31GDACFTJQQ+GhQNB86mxENvq4/kgPQRaVWrKgZcgebUoq4gx0MXq/sSZ2xu
    B5UOL3svXBjbXedals8moqUVO/yJUdQG0AW65UQEXm8m/JAIEFcAHZkR2KQb3rkz
    a7bu9JJ+AAIvgTmPAi7YcHNCjTDJlYXdCW7LQLiMTS9dH86uKUT1n+T7dM8+WxwH
    D3mRJuUVs6N7llOPUD9EQwZbS5gvb4k30Oc5DfsaupR4RNsLpU6Wop6lPBcRii24
    URgUJbQy6/XSEjJsh5UhDsESL6RCF/8UNTUfsK6QoD3c7TlAn5DLMO5GIlVrkUGj
    io2LSPKWENmXyk/46CsOsVD8q5TKcmLbiu1IQ22+A6qjoVmZDgs/aPTw91ZfaE5z
    AD6/q1oXqHyYfdSpIxA39fBZ3Np+i1z0rTmA5eJJ4o1ODnrVx7MOCgx7aQphnKxT
    T+UA7obU14RsMS7cIcb7y/+Q4xrrXTNETUzmJ7QMOCrMNMT7nHqCUJjXZnfk1nSf
    MjcZf9FcuQENBFYhmXgBCADPHjzmGIRoAzAmaPIEpkf8MQWrLrMSbXlzoXvch49W
    b0I7dRFzuZqvTdNLFx55qSSCicVuqZPWPwqSaeDoxBUOQYE3/wgkjuV6jRq42JqY
    LbOykp+mLtrnYCTi6vQbQPPYGmKAgOe6fy9od2KR31M/XTgFTfuw8K+mHZePnaa9
    y/cJ8Uwz63lP8GrjHHH2OknKqoVN6ZbfYX9JhCSuHoj3GeyxY1xUu/mt2hR+mkYj
    0vG9yi6ZVIIcOUHPOLGK2KKLK6u/xPQDcPU8ikMGZd8C1s03pNCgtJmtinUoXNSR
    vggwWipiLGQGZ6yfZV4CRE1UBKXZfzOfGWUFou8RjX63ABEBAAGJA0QEGAEKAA8F
    AlYhmXgCGwIFCQHhM4ABKQkQ8oT3oZRWrnzAXSAEGQEKAAYFAlYhmXgACgkQmQJ4
    rXYkMxSMUAf+PuFSIWXRdaXolGE5eFZDSo9UhTDrNHUg4Se3nSoW6qVsbVw3b7Ll
    ZvzYHYs6n38pgLHEAWQtY6V+g6uYgSd+TEeigyrwVxLbw338FE2xE/kH0VNk0jTs
    oUtVc5gMkltObRoxAKXNLci54Ox1bmLmCQpwtPZdfWba4JSIa8NMNpCGFnwjriQt
    zuojRHE6bw7Q7Clvg5Fiyj96sfjro9vmEGxhjuY7uTAwtHE+9VaZV88wktYICBcN
    7qbw79NfK2s8KdF+Ugx/dp909WxQxi4gTyvVLehArq+B2aQOMa1ZL+WCQkLam/IH
    DOzp0muHC3S001m+Fjrg6VCQYDZ8S4yc94gAEACKPJDgg5QRdJYGXNW3chE263SN
    q9e+Kxq1+T2hhcBpr37xvpWLiOZCjRcKnJJ5CBe7Kc/sAwxCWOEQSIscC7AL73eF
    y/KWFQmJm6D8MfnNsRV3X4BgkjliNm2cVWnN5sgFiLkAsyqrdE1EoDk5pBs4hfoJ
    +9judFzs5khO49+llNNKcQefPDmwcECrr2Ef8V9lv2fiABasSEuRCn3bRrYO9qSa
    66Dw6FDLkcCypmvtwdmWJavTA2KILxy9zH62QX356EFyc/Ek1SwEL1J2/lu1mnJF
    K+e5rARk++WibP7/+hZ5RjowcucFVxWK6LWdEITsaBdy49mxrxYq46DuHcmk4jK7
    LFq0VZ2hU2Un8FU1EhJTqXk07fpsQkj1qGpfUB/BqBy+ID6BUhY3D0l6135dniw9
    Pa1YsugCBxhUZstNp7en+YHAg6wB0NPmjdudl74acNSAGBBxDWdqYKngh54FJsYY
    Kj/qHxB8PBVcD03eCNzqniXC3lm14jNoKVDxjkexlym3VRwD/wjoPGPwFoXyumLO
    YcxnyZYlWDWT8jIMoYt6N+vi2RpCBnoBrK/rtLDuLGKNvr44+G1x0Iz+HXPrvpD8
    JGhhzOfQWwmSGhtaTW08Ultg/4dcSZp5LoGASnWdFOq7VjJXmxB1VTO1WoVPr0b5
    SS7ghTzJ8mCDiergArkBDQRWIZmiAQgAq2mU5T9IRbkrah7czjlgqtYUU3stknmL
    zrlCS/ChMVJa7ucDBtF1kcCGcKHgRa6Jn9zzBP3IVzi2id12bkpAiHCqNE+KoL1f
    oe1mE3sSTPJfNLJp0JpVhcv3KuZrKnxGbi5Ho5mx8kzLmumOiIl9iWe/1udaq2nE
    AoKIjNJrJN0cIxIRuWXOJV6t5I2i/NJgQG1PSpM1FaGOx3Rae48bNizhO8QjbcKd
    nYbjkWpntZ7AJ2KRDrfFCAL+4JIpiDpSNGpUAPh2Yuk/nWGT8TyfFJ8RSuFxN+Bk
    NjVWCQIymVUmT0fTFQo+KOL5ni5LKx4Bbwwa1U+lbyKjx8IpsRF3AwARAQABiQIl
    BBgBCgAPBQJWIZmiAhsMBQkB4TOAAAoJEPKE96GUVq58WmMP/1RmmIlLTaAScJGX
    tFyTBv5rX4j+geuOr6bBHvQNVk4FHt5rWu71Z5j3lEWBW0t0PF2NtfWEvnzFuWa7
    0IGB+zVzVyMddjTz4Bhg1639DmGLLZLSy93oBO+oEv82VCnDZHIZyYLIhbNzaLUE
    4oJMCTqhAarAaV0YrXRfOKFEUkPYbrjT2YQcyprAMD8vdSc75yBUZPCaYNWsAaE7
    G2awKaUitI08oJry3CTcboOQuKO+wxOnw11A+bF3LSpVBQ/G2vth97OfMKHQPbpH
    b7cTeCD1RBFvQkrrOkh1mRqVrjIGm17jYNSraRRyjYD6W00eMkXMi3yFrWTUVcq2
    mwwDSXaqROtx4wzHC8fzuG0qMoveLr6aMXiRALLESzophZh898I2/Q1MbMOM5rEU
    Dxb8iuBn/lxLPZlKMBubSydcDTUCQ6NOKpGS7zRBIzxhXdCvLJRwxUk2ASMXVcIU
    EjFRlS2glXZTyPVyNK4AO0D4v02bvwzRS39WuuKyY77It85ni3l9DgPiroEoyl/4
    5FBmO3tYkQFI0vqssUJ7wNs+UBbOrfheor8vxWpOL455RCoUgl/n7Jtgs3Pb7SCu
    +7MYV8Sj9vxQStHaowKEpooltDVCOAxBZzP9eguah/xJF4jDWRSlNRHhmmu2sS0P
    QtDnG+FHhabo6/Hj/wSUQPp2PU4BuQENBFYhmbkBCAC6DqHr9Sbnu76cgcXjcKT2
    +x3Uza0SyjVEy8dihN5AsE0eMNwoEyzkFOavYhUoUTlO8IfZe79V4ADw6lg4I6uF
    HHu/MQr6CbYCcCF4OHUEFhm0Wusw5jxBI7SkfpLJCdCkzZnbMA5vqrU5PRqAFRNS
    husMoSdcgO+DQA3zf3VZqkKk6QIQUQBsG0nluwtu+zuisC+KwS2/d8Mb5fkoznX0
    18pUFA1+kx04uvuXe6HlFtYZ6Oy9tpYDW4W5Bwg7huybZyExfXWQ/bHkiM0/a2rw
    yrzmhMu0xWCfXfNqO7tZyReK6yMgxtUD6K3BCq7/pjKS3blTf6RvWc2H/5vtcNoR
    ABEBAAGJAiUEGAEKAA8FAlYhmbkCGyAFCQHhM4AACgkQ8oT3oZRWrnx7GRAAno64
    5thjE2R5fYy/EYRHEeGHwuru2NHBpAIXQ23yMLNemLzVNX9svyx5xUH7W9mkUR9F
    Fquo8pN44Q3Ob5vUzLTmIQqrtcKBi0zl3PjB5fHACLJAPaLS3uhhF0Ox07SKSJFO
    YEl8/8dLMeRVALpkKx91QBMKqpX8sIHPE0rWdny2vHVYHjTzurFG2G0HRtJql1i+
    fHYIQtbYohoFduBfDqGE5OXZ+9ug3AFIJcS4/5dlj35DSayrqyGN+B21NBRot1bN
    so+aCfni5qhGPqX05qEPWA6ZDDGP3XhZ4Dre/qCctO2W0AqSUGYe78gwPoI1OlIk
    IMX3646qekYnMxJ36UIKJumhJNGoWc8D/gpCOig217uUhJKF3C7MzKFvW+3Yfg2r
    JmWz/xNQniQKOoWd69iJejUV3/gM3w4ZjjmnZ/vHwtY16RpHoooJhgZjsF51mFoQ
    w8JzPE1rH+X6JxO9SqNU+p0q+mdCdSYSghz/Qu5OBmu/dDKW7OqyyMvzsd3uyEe/
    ynzitFiMO2A1nGG1M/Fel9m9EIqVqQHZ5qQb7qACT2Sup/0182gRvWTcA4BjV6Kv
    tgtqyj5nv3fTZOLDLcFa9KNY1PfDwhgMxHWCij+AhGR1HMVYz0CvXbo4lij54SuL
    A86qRaICW53xHPLXSpeBUkibBmQCqrAt9tZ/kFo=
    =8nzf
    -----END PGP PUBLIC KEY BLOCK-----

*This document adapted from <https://rust-lang.org/security.html>*
